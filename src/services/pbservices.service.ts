import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PBService {
	constructor(private http: Http) { }

	fetchAllServices() {
  	let headers = new Headers();
  	headers.append('Content-Type','application/json');
  	return this.http.post('http://localhost:3000/pbservices/fetchAllServices', {headers: headers})
  	.map(res => res.json());
  }

  fetchPartnersByServiceId(serviceid) {
  	let headers = new Headers();
  	headers.append('Content-Type','application/json');
  	return this.http.post('http://localhost:3000/pbservices/fetchPartnersByServiceId', {serviceid: serviceid},
	  {headers: headers},)
  	.map(res => res.json());
  }
}