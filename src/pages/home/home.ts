import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {PBService} from '../../services/pbservices.service';
import {ChooseService} from '../choose-service/choose-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PBService]
})
export class HomePage {
 services: Array<any> = [];
  constructor(public navCtrl: NavController,
              public pbService: PBService) {
    this.fetchAllServices();
  }

    fetchAllServices() {
      this.pbService.fetchAllServices().subscribe(data => {
        this.services = data.services[0].SERVICE;        
      });
    }

    goToChooseService(selectedService) {
      var navOptions = {
       animation: 'ios-transition'
 	    };
	    this.navCtrl.push(ChooseService,{selectedService:selectedService},navOptions);
    }

}
