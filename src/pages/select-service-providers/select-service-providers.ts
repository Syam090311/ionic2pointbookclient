import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {PBService} from '../../services/pbservices.service';
import {CreateAppointmentPage} from '../create-appointment/appointmentdetails';

/**
 * Generated class for the SelectServiceProvidersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-select-service-providers',
  templateUrl: 'select-service-providers.html',
  providers: [PBService]
})
export class SelectServiceProvidersPage {
  partners: Array<any> = [];
  serviceidparam: any;
  serviceprice: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public pbService: PBService) {
  }

  dismiss() {
    var navOptions = {
        animation: 'ios-transition'
    };
    this.navCtrl.pop(navOptions);
  }

  ionViewDidLoad() {
    this.serviceidparam = this.navParams.get('serviceid');
    this.fetchPartnersByServiceId(this.serviceidparam);
  }

  fetchPartnersByServiceId(serviceid) {
      this.pbService.fetchPartnersByServiceId(serviceid).subscribe(data => {
        this.partners = data.partners;
        //Set Price
        for(let servicelist of this.partners) {
          for(let price of servicelist.PARTNER_SERVICES) {
            if(price.SERVICE_ID==serviceid) {
              this.serviceprice = price.COMMON_PRICE;
              break;
            }
          }
          break;
        }
      });
  }

  goToCreateAppointment(partnerData){
    var navOptions = {
      animation: 'ios-transition'
    }
    this.navCtrl.push(CreateAppointmentPage, {partnerData:partnerData}, navOptions);
  }

  onRatingChange(event) {

  }

}
