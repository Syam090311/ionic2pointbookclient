import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import {SelectServiceProvidersPage} from '../select-service-providers/select-service-providers';

@Component({
	templateUrl: 'choose-service.html',
})
export class ChooseService {
fullServiceList: Array<any> = [];
oneService: any;

 constructor(public params: NavParams,
 	public navCtrl: NavController) {

 	this.fullServiceList = this.params.get('selectedService').FULL_SERVICE_LIST;
	this.oneService = this.params.get('selectedService');

 }

 dismiss() {
	var navOptions = {
      animation: 'ios-transition'
 	};
	this.navCtrl.pop(navOptions);
 }

 selectServiceProvider(serviceid) {
	var navOptions = {
      animation: 'ios-transition'
 	};
	this.navCtrl.push(SelectServiceProvidersPage,{serviceid: serviceid},navOptions);
 }

}
