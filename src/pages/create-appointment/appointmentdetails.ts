import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CreateAppointmentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-create-appointment',
  templateUrl: 'appointmentdetails.html',
})
export class CreateAppointmentPage {
  public localDate: Date = new Date();
  public initDate: Date = new Date();
  public initDate2: Date = new Date(2015, 1, 1);
  // public disabledDates: Date[] = [new Date(2017, 7, 14)];

  public maxDate: Date = new Date(new Date().setDate(new Date().getDate() + 30));
  public min: Date = new Date();

  public apptTime: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) { 
  }

  dismiss() {
    var navOptions = {
        animation: 'ios-transition'
    };
    this.navCtrl.pop(navOptions);
  }

  ionViewDidLoad() {
    // this.localDate = new Date('2014-04-03');
    this.localDate = new Date();
    this.apptTime = this.calculateTime('+8');
  }

  public event(data: Date): void {
    this.localDate = data;
  }

  setApptDate(date: Date) {
    console.log(date);
    this.localDate = date;
    // this.initDate = date;
  }

  calculateTime(offset: any) {
    // create Date object for current location
    let d = new Date();

    // create new Date object for different city
    // using supplied offset
    let nd = new Date(d.getTime() + (3600000 * offset));

    return nd.toISOString();
  }

  // Determine if the client uses DST
  stdTimezoneOffset(today: any) {
    let jan = new Date(today.getFullYear(), 0, 1);
    let jul = new Date(today.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
  }

  dst(today: any) {
    return today.getTimezoneOffset() < this.stdTimezoneOffset(today);
  }
}
