import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CreateAppointmentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-create-appointment',
  templateUrl: 'appointmentdetails.html',
})
export class CreateAppointmentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  dismiss() {
    var navOptions = {
        animation: 'ios-transition'
    };
    this.navCtrl.pop(navOptions);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateAppointmentPage');
  }

}
