import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ChooseService } from '../pages/choose-service/choose-service';
import { SelectServiceProvidersPage } from '../pages/select-service-providers/select-service-providers';
import { CreateAppointmentPage } from '../pages/create-appointment/appointmentdetails';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpModule } from '@angular/http';

import { Ionic2RatingModule } from 'ionic2-rating';
import { DatePickerModule } from 'datepicker-ionic2';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChooseService,
    SelectServiceProvidersPage,
    CreateAppointmentPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    Ionic2RatingModule ,
    DatePickerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChooseService,
    SelectServiceProvidersPage,
    CreateAppointmentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
